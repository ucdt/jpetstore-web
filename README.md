# jpetstore-web

This repository holds the source code for jpetstore-web 1.0 and 1.1. jpetstore-web
contains the static content for JPetStore and is deployed to the exploded WAR file
ont Apache Tomcat webapps/JPetStore. The versions are managed by branches release_1.0
and release_1.1.